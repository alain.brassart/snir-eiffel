# Ressources pour les cours et TP d'info du BTS

> - Auteur : Alain BRASSART
> - Date de la dernière publication : 15/11/2021
> - OS: Windows 10 (version 20H2)

![CC-BY-NC-SA](img/cc-by-nc-sa-150.png) [Licence Creative Commons](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/)

## 1. Développement Web

* [Développer en JavaScript](Développement_Web/1_JavaScript/readme.md)
  